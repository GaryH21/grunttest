module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		cssmin: {
			build: {
				files: {
					'build/css/style.min.css': 'dev/css/*.css'
				}
			}
		},
		uglify: {
			build: {
				files: {
        			'build/js/scripts.min.js': ['dev/js/*.js']
      			}
			}
		},
		coffee: {
			build: {
    			files: {
					'dev/js/scripts.js': 'dev/coffee/*.coffee'
				}
  			}
		},
		compass: {
			build: {
				options: {
					sassDir: 'dev/scss',
					cssDir: 'dev/css'	
      			}
			}
		},
		watch: {
			scripts: {
    			files: ['dev/coffee/*.coffee'],
    			tasks: ['coffee', 'jshint', 'uglify'],
    			options: {
      				livereload: 35729
    			}
    		},
    		styles: {
    			files: ['dev/scss/*.scss'],
    			tasks: ['compass', 'cssmin'],
    			options: {
      				livereload: 35729
    			}
    		},
    		bootstrap: {
    			files: ['dev/scss/bootstrap/*.scss'],
    			tasks: ['compass', 'cssmin'],
    			options: {
      				livereload: 35729
    			}
    		},
			html: {
    			files: ['dev/*.html'],
    			tasks: ['htmlmin'],
    			options: {
      				livereload: 35729
    			}
    		},
    		tests: {
    			files: ['dev/tests/**/*.html'],
    			tasks: ['mocha'],
    			options: {
      				livereload: 35729
    			}
    		}
		},
		htmlmin: {
			dev: {
				options: {
        			removeComments: true,
        			collapseWhitespace: true
      			},
      			files: {
        			'build/index.html': 'dev/index.html'
      			}
    		}
		},
		jshint: {
    		build: {
    			files: {
    				src: ['dev/js/scripts.js']
    			}
    		}
    		
  		},
  		connect: {
    		server: {
      			options: {
        			port: 8000,
        			hostname: '*',
        			base: 'build',
        			onCreateServer: function(server, connect, options) {
          				var io = require('socket.io').listen(server);
          				io.sockets.on('connection', function(socket) {
            				// do something with socket
          				});
        			}
      			}
    		},
    		testing: {
      			options: {
        			port: 8001,
        			hostname: '*',
        			base: 'dev/tests/default/',
        			onCreateServer: function(server, connect, options) {
          				var io = require('socket.io').listen(server);
          				io.sockets.on('connection', function(socket) {
            				// do something with socket
          				});
        			}
      			}
    		}
  		},
  		mocha: {
  			test: {
    			src: ['dev/tests/**/*.html'],
    			options: {
      				growlOnSuccess: true,
      				run: true
    			}
			}
		}
	});

	grunt.registerTask('default', ['compass','cssmin','coffee','jshint','uglify','htmlmin','mocha','connect','watch'])

}